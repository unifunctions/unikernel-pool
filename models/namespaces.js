const mongoose = require('mongoose');

const Namespace = mongoose.model('Namespace', new mongoose.Schema({
  name: {
    type: String,
    required: true,
    index: true,
  },
  uid: {
    type: mongoose.Schema.Types.ObjectId,
    required: true,
    index: true,
    ref: 'User',
  }
}));

const create = (data) => {
  return Namespace.create(data);
}

const getByName = (name) => {
  return Namespace.findOne({ name }).exec();
}

const getById = (id) => {
  return Namespace.findById(id).exec();
}

const getByUser = (uid) => {
  return Namespace.find({ uid }).exec();
}

const updateName = (conditions, name) => {
  return Namespace.findOneAndUpdate(conditions, { name }, { new: true }).exec();
}

// const updateUser = (conditions, newUid) => {
//   return Namespace.findOneAndUpdate(conditions, { uid: newUid }).exec();
// }

const remove = (conditions) => {
  return Namespace.findOneAndDelete(conditions).exec();
}

module.exports = { create, getByName, getById, getByUser, updateName, remove }