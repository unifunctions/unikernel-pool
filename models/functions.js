const mongoose = require('mongoose');

const Function = mongoose.model('Function', new mongoose.Schema({
  name: {
    type: String,
    required: true,
    index: true,
  },
  runtime: {
    type: String,
    required: true,
  },
  uid: { // _id of the user
    type: mongoose.Types.ObjectId,
    required: true,
    index: true,
    ref: 'User',
  },
  nid: { // _id of the namespace
    type: mongoose.Types.ObjectId,
    required: true,
    index: true,
    ref: 'Namespace',
  },
  api: {
    type: String,
    unique: true,
    index: true,
    default: '',
  }
}));

/**
 * Create a Function and save its information in MongoDB.
 * @param {Object} info Information of the Function.
 */
const create = (info) => {
  return Function.create({
    ...info,
  });
}

/**
 * Find a Function with certain conditions and return a Promise.
 * @param {Object} conditions Conditions for finding a Function
 */
const get = (conditions) => {
  return Function.findOne(conditions).exec();
}

/**
 * Find all the Functions that satisfy certain conditions.
 * @param {Object} conditions Conditions for find the functions.
 */
const getMany = (conditions) => {
  return Function.find(conditions).exec();
}

/**
 * Find a Function in MongoDB with its _id and return a Promise.
 * @param {String} id _id of the Function to be found.
 */
const getById = (id) => {
  return Function.findById(id).exec();
}

/**
 * Find a Function by its name and return a Promise.
 * @param {String} name Name of the Function to be found.
 */
const getByName = (name) => {
  return Function.findOne({ name }).exec();
}

/**
 * Find a Function by its API endpoint and return a Promise.
 * @param {String} api API of the Function to be found.
 */
const getByApi = (api) => {
  return Function.findOne({ api }).exec();
}

/**
 * Find a Function by the _id of its Namespace.
 * @param {String} nid _id of the Namespace of the Function to be found.
 */
const getByNamespace = (nid) => {
  return Function.find({ nid }).exec();
}
/**
 * Find a Function with certain conditions and updates its information, then return a Promise.
 * @param {Object} conditions Conditions for finding a Function.
 * @param {Object} updates Updates to be made to the Function in MongoDB.
 */
const update = (conditions, updates) => {
  return Function.findOneAndUpdate(conditions, update, { new: true }).exec();
}

/**
 * Find a Function with its name, delete it from MongoDB and return a Promise.
 * @param {Object} conditions Name of the Function to be deleted.
 */
const deleteByName = (name) => {
  return Function.deleteOne({ name }).exec();
}

/**
 * Find a Function with its _id, delete it from MongoDB and return a Promise.
 * @param {String} id _id of the Function to be found.
 */
const deleteById = (id) => {
  return Function.findByIdAndDelete(id).exec();
}

module.exports = { create, get, getMany, getById, getByName, getByNamespace, getByApi, update, deleteByName, deleteById };