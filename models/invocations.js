const mongoose = require('mongoose');

const Invocation = mongoose.model('Invocation', new mongoose.Schema({
  success: {
    type: Boolean,
    required: true,
  },
  fid: {
    type: mongoose.Schema.Types.ObjectId,
    required: true,
    index: true,
    ref: 'Function',
  },
  execution_time: {
    type: Number,
    required: true,
  },
  result: {
    type: Object,
    required: true,
  }
}));

/**
 * Create an Invocation record in MongoDB and return a Promise.
 * @param {Object} info Information of the Invocation.
 */
const create = (info) => {
  return Invocation.create(info);
}

/**
 * Find an Invocation record with certain conditions and return a Promise.
 * @param {Object} conditions Conditions for finding an Invocation record.
 */
const get = (conditions) => {
  return Invocation.findOne(conditions).exec();
}

/**
 * Find an Invocation record with its _id and return a Promise.
 * @param {String} id _id of the Invocation record to be found.
 */
const getById = (id) => {
  return Invocation.findById(id).exec();
}

/**
 * Find an Invocation record, update its information and return a Promise.
 * @param {Object} conditions Conditions for finding an Invocation record.
 * @param {Object} updates Updates to be made to the Invocation record.
 */
const update = (conditions, updates) => {
  return Invocation.findOneAndUpdate(conditions, updates, { new: true }).exec();
}

/**
 * Find an Invocation record with certain conditions, delete it from MongoDB and return a Promise.
 * @param {Object} conditions Conditions for finding an Invocation record.
 */
const deleteByConditions = (conditions) => {
  return Invocation.findOneAndDelete(conditions).exec();
}

/**
 * Find an Invocation record with its _id, delete it from MongoDB and return a Promise.
 * @param {String} id _id of the Invocation record to be found.
 */
const deleteById = (id) => {
  return Invocation.findByIdAndDelete(id).exec();
}

module.exports = { create, get, getById, update, deleteByConditions, deleteById };
