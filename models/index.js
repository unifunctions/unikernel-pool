const functionModel = require('./functions');
const namespaceModel = require('./namespaces');
const invocationModel = require('./invocations');

module.exports = { functionModel, namespaceModel, invocationModel }