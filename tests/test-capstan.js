const { init, compose, run } = require('../tools/capstanHelper');
const { npmInstall } = require('../tools/setRuntime');
const config = require('../config');
const path = require('path');

const namespace = 'testNamespace';
const funcName = 'testFunction';

const name = `${namespace}.${funcName}`;

const meta = {
  package: {
    name,
    title: name,
    author: namespace,
    require: 'node-6.10.2'
  },
  run: {
    runtime: 'native',
    config_set: {
      main: {
        bootcmd: '/node /index.js',
      }
    },
    config_set_default: 'main'
  }
}

const directory = path.join(config.packages, namespace, funcName);

const testInit = async () => {
  try {
    const result = await init(meta, directory);
    console.log(result);
  } catch (err) {
    console.warn(`${err}`);
  }
}

const testNpm = async () => {
  try {
    const result = await npmInstall(directory);
    console.log(result);
  } catch (err) {
    console.warn(`${err}`);
  }
}

const testCompose = async () => {
  try {
    const result = await compose(name, directory);
    console.log(result);
  } catch (err) {
    console.warn(`${err}`)
  }
}

const testRun = async () => {
  try {
    const args  = '{"exclude_tags": ["racist", "sex", "dirty", "insult"] }'.replace(/"/g, '\\"');
    const execute = `/node invoker.js "${args}"`;
    const start = process.hrtime();
    const result = await run(name, execute);
    const end = process.hrtime(start);
    console.log(`Time: ${end[1] / 1000000}ms`);
    if (result.type === 'stdout') {
      console.log(result.output[5].slice(7));
    } else {
      throw result;
    }
  } catch (err) {
    console.warn(err);
  }
}

const func = async () => {
  console.log('Initializing unikernel...');
  await testInit();
  // console.log('\n\nInstalling dependencies...');
  // // await testNpm();
  console.log('\n\nComposing unikernel...');
  await testCompose();
  console.log('\n\nStarting unikernel...');
  await testRun();
}

// testRun();

func();
// const test = async () => {
//   const start = process.hrtime();
//   await testRun();
//   const end = process.hrtime(start);
//   console.log(end[1] / 1000000 + 'ms');
// }

// test();

// testNpm();