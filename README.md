# Unifunctions

Core functionalities of Unifunctions.

## Requirements

- Node.js
- MongoDB
- QEMU

## Installation

Direct to the project folder and execute:
```sh
npm install
```

## Execution

Direct to the project folder and execute:
```sh
npm start
```

## Usage

The pool provides REST APIs and can be invoked as:
```sh
curl -X POST http://localhost:3000/namespace/:ns/functions/:fn
```
