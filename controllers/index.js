const functionsController = require('./functions');
const namespacesController = require('./namespaces');

module.exports = { functionsController, namespacesController }