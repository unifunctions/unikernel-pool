const path = require('path');
const { init, compose, run, removeImage, genUnikernelMeta, genExecuteCommand } = require('../tools/capstanHelper');
const { functionModel, namespaceModel, invocationModel } = require('../models');
const { createError } = require('../tools/utils');
const config = require('../config');

const create = async (info) => {
  return new Promise(async (resolve, reject) => {
    try {
      const { userNamespace, functionName, runtime } = info;
      const namespace = await namespaceModel.getByName(userNamespace);
      if (!namespace) {
        throw createError(`Namespace not found: ${name}`, 404);
      }
      const name = `${userNamespace}.${functionName}`;
      if (await functionModel.getByName(name)) {
        throw createError(`Function exists: ${name}`, 409);
      }
      const meta = genUnikernelMeta(info);
      const directory = path.join(config.packages, userNamespace, functionName);
      await init(meta, directory);
      // await npmInstall(directory);
      await compose(meta.package.name, directory);
      const func = await functionModel.create({
        name,
        nid: namespace._id,
        uid: namespace.uid,
        runtime,
      });
      resolve(func);
    } catch (err) {
      reject(err);
    }
  });
}

const update = async (userNamespace, functionName, updates) => {
  // todo
}

const get = async (userNamespace, functionName) => {
  const name = `${userNamespace}.${functionName}`;
  return new Promise(async (resolve, reject) => {
    try {
      const func = await functionModel.getByName(name);
      if (func) {
        resolve(func);
      } else {
        throw createError(`Function not found: ${name}`, 404);
      }
    } catch (err) {
      reject(err);
    }
  });
}

const remove = async (userNamespace, functionName) => {
  const name = `${userNamespace}.${functionName}`;
  return new Promise(async (resolve, reject) => {
    try {
      await removeImage(name);
      const func = await functionModel.deleteByName(name);
      resolve(func);
    } catch (err) {
      reject(err);
    }
  });
}

const invoke = async (userNamespace, functionName, payload) => {
  const name = `${userNamespace}.${functionName}`;
  return new Promise(async (resolve, reject) => {
    try {
      // Need to improve performance.
      const func = await functionModel.getByName(name);
      if (!func) {
        throw createError(`Function not found: ${name}`, 404);
      }
      const execute = genExecuteCommand(func.runtime, payload);
      const result = await run(name, execute);
      if (result.type === 'stdout') {
        const res = JSON.parse(result.output[5].slice(4)); // The result is in the format of "RES:******" or "ERR:******" so the first 3 characters need to be deleted.
        await invocationModel.create({
          nid: func.nid,
          ...res,
        });
        if (!res.success) {
          throw JSON.parse(res.result);
        }
        resolve(res.result);
      } else {
        throw createError('Error invoking function.', 500);
      }
    } catch (err) {
      reject(err);
    }
  });
}

const getInvocation = async (userNamespace, id) => {
  return new Promise(async (resolve, reject) => {
    try {
      const namespace = await namespaceModel.getByName(userNamespace);
      if (!namespace) {
        throw createError(`Namespace not found: ${userNamespace}`, 404);
      }
      const invocation = await invocationModel.get({ nid: namespace._id, id });
      if (!invocation) {
        throw createError(`Invocation not found: ${id}`, 404);
      }
      resolve(invocation);
    } catch (err) {
      reject(err);
    }
  });
}

module.exports = { create, update, get, remove, invoke, getInvocation };