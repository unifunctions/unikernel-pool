const { namespaceModel, functionModel } = require('../models');
const { createError } = require('../tools/utils');

const create = (uid, name) => {
  return new Promise(async (resolve, reject) => {
    try {
      const namespace = await namespaceModel.create({ uid, name });
      resolve(namespace);
    } catch (err) {
      if (err.name == 'MongoError' && err.code == 11000) {
        err = createError(`Namespace exists: ${name}`, 409);
      }
      reject(err);
    }
  });
}

const updateName = (name, newName) => {
  return new Promise(async (resolve, reject) => {
    try {
      const namespace = await namespaceModel.updateName({ name }, newName);
      resolve(namespace);
    } catch (err) {
      reject(err);
    }
  });
}

const get = (name) => {
  return new Promise(async (resolve, reject) => {
    try {
      const namespace = await namespaceModel.getByName(name);
      resolve(namespace);
    } catch (err) {
      reject(err);
    }
  });
}

const getFunctions = (name) => {
  return new Promise(async (resolve, reject) => {
    try {
      const namespace = await namespaceModel.getByName(name);
      const funcs = await functionModel.getByNamespace(namespace._id);
      resolve(funcs);
    } catch (err) {
      reject(err);
    }
  });
}

const remove = (name) => {
  return new Promise(async (resolve, reject) => {
    try {
      await namespaceModel.remove({ name });
      resolve({ message: 'Namespace deleted.' });
    } catch (err) {
      reject(err);
    }
  });
}

module.exports = { create, updateName, get, getFunctions, remove };