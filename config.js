const config = {
  home: '/home/unifunctions',
  packages: '/home/unifunctions/unifunctions/packages',
  images: '/home/unifunctions/unifunctions/images',
  mongodb: 'mongodb://localhost:27017/unifunctions',
  runtime: {
    node: {
      v4: {
        node: '/home/unifunctions/.nvm/versions/node/v4.4.5/bin/node',
        npm: '/home/unifunctions/.nvm/versions/node/v4.4.5/bin/npm',
      },
      v6: {
        node: '/home/unifunctions/.nvm/versions/node/v6.17.1/bin/node',
        npm: '/home/unifunctions/.nvm/versions/node/v4.4.5/bin/npm',
      },
    }
  }
}

module.exports = config;