const express = require('express');
const path = require('path');
const cookieParser = require('cookie-parser');
const logger = require('morgan');
const mongoose = require('mongoose');
const mongodb = require('./config').mongodb;
const { createError } = require('./tools/utils');

const indexRouter = require('./routes/index');
const apiRouter = require('./routes/api');

mongoose.connect(mongodb, { useNewUrlParser: true });
const dbConnection = mongoose.connection;
dbConnection.on('error', () => {
  console.log('Error connecting to MongoDB');
});

const app = express();

// view engine setup
app.set('views', path.join(__dirname, 'views'));
app.set('view engine', 'jade');

app.use(logger('dev'));
app.use(express.json());
app.use(express.urlencoded({ extended: false }));
app.use(cookieParser());
app.use(express.static(path.join(__dirname, 'public')));

app.use('/', indexRouter);
app.use('/api', apiRouter);

// catch 404 and forward to error handler
app.use((req, res, next) => {
  next(createError('Not found', 404));
});

// error handler
app.use((err, req, res, next) => {
  // set locals, only providing error in development
  res.locals.message = err.message;
  res.locals.error = req.app.get('env') === 'development' ? err : {};

  res.status(err.statusCode || 500).json(err.errMessage || { errMessage: 'Internal Server Error.' });
});

module.exports = app;
