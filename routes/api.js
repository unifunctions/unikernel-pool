const router = require('express').Router();
const { functionsController, namespacesController } = require('../controllers');
const { createError } = require('../tools/utils');

// Create a new Namespace.
router.post('/namespaces/:userNamespace', async (req, res) => {
  const uid = req.body.uid;
  const userNamespace = req.params.userNamespace;
  try {
    const namespace = await namespacesController.create(uid, userNamespace);
    res.status(201).json(namespace);
  } catch (err) {
    if (err.statusCode) {
      res.status(err.statusCode).json({ errMessage } = err);
    } else {
      res.status(500).json({ errMessage: 'Internal Server Error.' });
    }
  }
});

// Create a new Function under a Namespace
router.put('/namespaces/:userNamespace/functions/:functionName', async (req, res) => {
  const { action } = req.query;
  const payload = req.body;
  if (!action) {
    res.status(400).json({ errMessage: 'Action required.' });
  } else if (!payload) {
    res.status(400).json({ errMessage: 'Payload required.' });
  } else {
    const { userNamespace, functionName } = req.params;
    try {
      switch (action) {
        case 'create':
          const func = await functionsController.create({
            userNamespace,
            functionName,
            runtime: payload.runtime,
          });
          res.status(201).json(func);
          break;
        case 'update':
          // todo
          break;
        default:
          throw createError('Invalid action.', 400);
      }
    } catch (err) {
      if (err.statusCode) {
        res.status(err.statusCode).json({ errMessage } = err);
      } else {
        res.status(500).json({ errMessage: 'Internal Server Error.' });
      }
    }
  }
});

// Get a Function under a Namespace.
router.get('/namespaces/:userNamespace/functions/:functionName', async (req, res) => {
  const { userNamespace, functionName } = req.params;
  try {
    const func = await functionsController.get(userNamespace, functionName);
    res.status(200).json(func);
  } catch (err) {
    if (err.statusCode) {
      res.status(err.statusCode).json({ errMessage } = err);
    } else {
      res.status(500).json({ errMessage: 'Internal Server Error.' });
    }
  }
});

// Delete a Function under a Namespace.
router.delete('/namespaces/:userNamespace/functions/:functionName', async (req, res) => {
  const { userNamespace, functionName } = req.params;
  try {
    await functionsController.remove(userNamespace, functionName);
    res.status(204).end();
  } catch (err) {
    res.status(500).json({ errMessage: 'Internal Server Error.' });
  }
});

// Invoke a Function under a Namespace.
router.post('/namespaces/:userNamespace/functions/:functionName', async (req, res) => {
  const { userNamespace, functionName } = req.params;
  const payload = req.body;
  try {
    const result = await functionsController.invoke(userNamespace, functionName, payload);
    res.status(200).json(result);
  } catch (err) {
    if (err.statusCode) {
      res.status(err.statusCode).json({ errMessage } = err);
    } else {
      res.status(500).json({ errMessage: 'Internal Server Error.' });
    }
  }
});

// Get the execution result of an Invocation.
router.get('/namespace/:userNamespace/invocations/:invocationId', async (req, res) => {
  const { userNamespace, invocationId } = req.params;
  try {
    const invocation = functionsController.getInvocation(userNamespace, invocationId);
    res.status(200).json(invocation);
  } catch (err) {
    if (err.statusCode) {
      res.status(err.statusCode).json({ errMessage } = err);
    } else {
      res.status(500).json({ errMessage: 'Internal Server Error.' });
    }
  }
});

module.exports = router;