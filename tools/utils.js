const { exec } = require('child_process');
const fs = require('fs');
const { cwd } = require('process');

/**
 * Executes a shell command and returns a Promise.
 * @param {String} cmd Command line to be executed
 * @param {String} directory Directory where the command will be executed.
 */
const execCommand = (cmd, directory = cwd()) => {
  return new Promise((resolve, reject) => {
    exec(cmd, { cwd: directory }, (error, stdout, stderr) => {
      if (error) {
        reject(error);
      } else {
        resolve(stdout ? { type: 'stdout', output: stdout.split('\n') } : { type: 'stderr', output: stderr.split('\n') });
      }
    });
  });
}

/**
 * Writes data to a file and returns a Promise.
 * @param {String} path Path of the file to be written to.
 * @param {String} data Data to be written to the file.
 */
const writeFile = (path, data) => {
  return new Promise((resolve, reject) => {
    fs.writeFile(path, data, (err) => {
      reject(err);
    });
    resolve();
  });
}

/**
 * Create an error Object
 * @param {String} errMessage 
 * @param {Number} statusCode Default: 500
 */
const createError = (errMessage, statusCode=500) => {
  return {
    statusCode,
    errMessage,
  }
}

module.exports = { execCommand, writeFile, createError };