const { execCommand } = require('./utils');
const { runtime } = require('../config');

/**
 * Install Node.js modules in a directory and return a Promise.
 * @param {String} directory Directory where node_modules will be installed.
 */
const npmInstall = (directory) => {
  return new Promise(async (resolve, reject) => {
    try {
      const npm = runtime.node.v6.npm;
      const cmd = `${npm} install`;
      const result = await execCommand(cmd, directory);
      resolve(result);
    } catch (err) {
      reject(err);
    }
  });
}

module.exports = { npmInstall };

