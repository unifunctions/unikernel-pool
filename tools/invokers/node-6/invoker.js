var handler = require('./handler');

try {
  var args = JSON.parse(process.argv.slice(2)[0]);
  var startTime = process.hrtime();
  var result = args ? handler(args) : handler();
  var execution_time = process.hrtime(startTime)[1];
  var invocation = {
    success: true,
    execution_time,
    result,
  }
  console.log('RES:' + JSON.stringify(invocation));
} catch (err) {
  var invocation = {
    success: false,
    err: JSON.stringify(err),
  }
  console.log('ERR:' + JSON.stringify(invocation));
}

// node invoker.js "{\"exclude_tags\": [\"racist\", \"sex\", \"dirty\", \"insult\"] }"

process.exit(0);