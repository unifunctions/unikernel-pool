const path = require('path');
const json2yaml = require('json2yaml');
const { execCommand, writeFile } = require('./utils');

/**
 * Initialize a unikernel and return a Promise.
 * @param {Object} meta Meta data required to create the unikernel.
 * @param {String} directory The directory of the package to be run inside this unikernel.
 */
const init = async (meta, directory) => {
  return new Promise(async (resolve, reject) => {
    try {
      const cmd = `capstan package init --name "${meta.package.name}" --title "${meta.package.title}" --author "${meta.package.author}" --require "${meta.package.require}"`;
      const result = await execCommand(cmd, directory);
      const yamlStr = json2yaml.stringify(meta.run);
      await writeFile(path.join(directory, '/meta', '/run.yaml'), yamlStr);
      resolve(result);
    } catch (err) {
      reject(err);
    }
  });
}

/**
 * Compose a unikernel and return a Promise.
 * @param {String} name The name of the unikernel to be composed.
 * @param {String} directory The directory of the package to be run inside this unikernel.
 */
const compose = (name, directory) => {
  return new Promise(async (resolve, reject) => {
    try {
      const cmd = `capstan package compose "${name}" --pull-missing`;
      const result = await execCommand(cmd, directory);
      resolve(result);
    } catch (err) {
      reject(err);
    }
  });
}

/**
 * Run a unikernel and return a Promise.
 * @param {String} name The name of the unikernel to be started.
 * @param {String} execute The command to be executed in the Unikernel.
 */
const run = (name, execute=null) => {
  return new Promise(async (resolve, reject) => {
    try {
      const cmd = execute ? `capstan run "${name}" --execute '${execute}'` : `capstan run "${name}"`;
      const result = await execCommand(cmd);
      resolve(result);
    } catch (err) {
      reject(err);
    }
  });
}

/**
 * Remove an unikernel image and return a Promise.
 * @param {String} name Name of the image to be removed.
 */
const removeImage = (name) => {
  return new Promise(async (resolve, reject) => {
    try {
      const cmd = `capstan rmi ${name}`;
      const result = await execCommand(cmd);
      resolve(result);
    } catch (err) {
      reject(err);
    }
  });
}

const genUnikernelMeta = (info) => {
  const { functionName, userNamespace, runtime } = info;
  const package = {
    name: `${userNamespace}.${functionName}`,
    title: `${userNamespace}.${functionName}`,
    author: userNamespace,
  }
  switch (runtime) {
    case 'node-6':
      return {
        package: { ...package, require: 'node-6.10.2' },
        run: {
          runtime: 'native',
          config_set: {
            default: { bootcmd: '/node /invoker.js' }
          },
          config_set_default: 'default',
        }
      }
    default:
      throw new Error({ errMessage: `Runtime not supported: ${runtime}.` });
  }
}

const genExecuteCommand = (runtime, args) => {
  switch (runtime) {
    case 'node-6':
      return `/node invoker.js "${JSON.stringify(args).replace(/"/g, '\\"')}"`;
    default:
      throw new Error({ errMessage: `Runtime not supported: ${runtime}` });
  }
}

module.exports = { init, compose, run, removeImage, genUnikernelMeta, genExecuteCommand };